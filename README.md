This repo contains five files:

| File                | Description                                                                       |
|---------------------|-----------------------------------------------------------------------------------|
| `fake_big_file.lfs` | A copy of `fake_big_file.txt` that uses LFS                                       |
| `fake_big_file.txt` | A copy of `fake_big_file.lfs` that doesn't use LFS                                |
| `.gitattributes`    | Instructs git to use LFS for all files ending in `.lfs`                           |
| `README.md`         | This file                                                                         |
| `test.sh`           | Checks to see if `fake_big_file.lfs` has the same content as `fake_big_file.txt`  |

Run `./test.sh`, it will output either:
* "Files fake_big_file.txt and fake_big_file.lfs are the same" and exit with 0
    - This means the repository was successfully cloned with LFS
* "Files fake_big_file.txt and fake_big_file.lfs differ" and exit with 1
    - This means the repository was not cloned with LFS

