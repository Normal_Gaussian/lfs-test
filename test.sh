#!/bin/sh

local green='\033[0;32m'
local red='\033[0;31m'
local off='\033[0;00m'

file1=fake_big_file.lfs
file2=fake_big_file.txt

if git lfs > /dev/null 2>&1; then
  echo "${green}Git LFS is installed"
else
  echo "${red}Git LFS is not installed"
fi

if diff --brief "$file1" "$file2" > /dev/null 2>&1; then
  echo "${green}Repository was cloned with Git LFS"
else
  exit "${red}Repository was not cloned with Git LFS"
fi
